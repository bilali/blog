package com.siwaak.blog.controller;

import com.siwaak.blog.entity.Poste;
import com.siwaak.blog.service.PosteService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController("/api/postes")
public class PosteController {

    private final PosteService posteService;

    public PosteController(PosteService posteService) {
        this.posteService = posteService;
    }

    @GetMapping
    public List<Poste> getAllPostes(){
        return posteService.getAllPostes();
    }


    @GetMapping("/{id}")
    public Optional<Poste> getPoste(@PathVariable(value = "id") Long id){
        return posteService.getPoste(id);
    }

    @PostMapping
    public Poste createPoste(@RequestBody Poste poste){
        return  posteService.createPoste(poste);
    }

    @PutMapping("/{id}")
    public Poste updatePoste(@PathVariable(value = "id") Long id, @RequestBody Poste poste){
        poste.setId(id);
        return  posteService.updatePoste(poste);
    }

    @DeleteMapping("/{id}")
    public  void deletePoste(@PathVariable(value = "id") Long id){
        posteService.deletePoste(id);
    }
}
