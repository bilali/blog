package com.siwaak.blog.util;

import com.github.javafaker.Faker;
import com.siwaak.blog.entity.Poste;
import com.siwaak.blog.service.PosteService;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class DatabaseSeeder {

    private Faker faker;

    private final PosteService posteService;

    public DatabaseSeeder( PosteService posteService) {
        this.faker = new Faker(new Locale("fr"));
        this.posteService = posteService;
    }


    @EventListener
    public void seed(ContextRefreshedEvent contextRefreshedEvent){
        seedPosteTable();

    }

    public void seedPosteTable(){
        Poste poste = new Poste();

        for(int i=1;i<50;i++) {

            poste.setId((long) i);
            poste.setTitre(faker.app().name());
            poste.setContenue(faker.lorem().paragraphs(6).toString());

            posteService.createPoste(poste);
        }

        posteService.getPoste((long) 5).ifPresent(poste1 -> {
            System.out.println(poste1.getContenue());
        });
    }
}
