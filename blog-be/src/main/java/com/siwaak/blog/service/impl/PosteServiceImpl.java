package com.siwaak.blog.service.impl;

import com.siwaak.blog.entity.Poste;
import com.siwaak.blog.repository.PosteRepository;
import com.siwaak.blog.service.PosteService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PosteServiceImpl implements PosteService {

    private  final PosteRepository posteRepository;

    public PosteServiceImpl(PosteRepository posteRepository) {
        this.posteRepository = posteRepository;
    }


    @Override
    public List<Poste> getAllPostes() {
        return posteRepository.findAll();
    }

    @Override
    public Optional<Poste> getPoste(Long id) {
        return posteRepository.findById(id);
    }

    @Override
    public Poste createPoste(Poste poste) {
        return posteRepository.save(poste);
    }

    @Override
    public Poste updatePoste(Poste poste) {
        return posteRepository.save(poste);
    }

    @Override
    public void deletePoste(Long id) {
        posteRepository.deleteById(id);
    }
}
