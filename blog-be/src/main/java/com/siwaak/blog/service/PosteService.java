package com.siwaak.blog.service;

import com.siwaak.blog.entity.Poste;

import java.util.List;
import java.util.Optional;

public interface PosteService {

    public List<Poste> getAllPostes();

    public Optional<Poste> getPoste(Long id);

    public Poste createPoste(Poste poste);

    public Poste updatePoste(Poste poste);

    public void deletePoste(Long id);
}
