package com.siwaak.blog.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Lob;


@Getter
@Setter
@Entity
public class Poste extends BaseEntity {

    String titre;
    @Lob
    String contenue;

}
